/*
 * ImdNode.nc - Configuration for Imd node
 * Team member: 1.Thai Tuan
 *              2. Duy Huynh
 *              3. Rithea Ngeth
 *              4. HO Tuan Vu
 * JAIST, Jul 9, 2016
 */
#include "appFeatures.h"
includes sensorboardApp;

configuration ImdNode {
}
implementation {
  components Main, GenericCommPromiscuous as Comm, 
  MULTIHOPROUTER, ImdNodeM, LedsC, TimerC;  
 
  Main.StdControl -> ImdNodeM.StdControl;
  Main.StdControl -> Comm.Control;
  Main.StdControl -> MULTIHOPROUTER.StdControl;
  Main.StdControl ->TimerC.StdControl;
  
  ImdNodeM.Leds -> LedsC.Leds;
  ImdNodeM.Timer -> TimerC.Timer[unique("Timer")];
  
  // Wiring for XMesh
  ImdNodeM.RouteControl -> MULTIHOPROUTER;
  ImdNodeM.Send -> MULTIHOPROUTER.MhopSend[AM_XMULTIHOP_MSG];
  MULTIHOPROUTER.ReceiveMsg[AM_XMULTIHOP_MSG] ->Comm.ReceiveMsg[AM_XMULTIHOP_MSG];   
}

