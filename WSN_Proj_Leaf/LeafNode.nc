/*
 * LeafNode.nc - Configuration for leaf node
 * Team member: 1.Thai Tuan
 *              2. Duy Huynh
 *              3. Rithea Ngeth
 *              4. HO Tuan Vu
 * JAIST, Jul 9, 2016
 */
#include "appFeatures.h"
includes sensorboardApp;

configuration LeafNode {
}
implementation {
  components Main, GenericCommPromiscuous as Comm, 
	MULTIHOPROUTER, LeafNodeM, Voltage,
	TimerC, LedsC, SensirionHumidity;
  
  Main.StdControl -> TimerC.StdControl;
  Main.StdControl -> LeafNodeM.StdControl;
  Main.StdControl -> Comm.Control;
  Main.StdControl -> MULTIHOPROUTER.StdControl;
  
  LeafNodeM.Timer -> TimerC.Timer[unique("Timer")];
  LeafNodeM.Leds -> LedsC.Leds;
  
  // Wiring for Battery Ref
  LeafNodeM.BattControl -> Voltage;  
  LeafNodeM.ADCBatt -> Voltage;
  
  // Wiring for Sensirion sensor
  LeafNodeM.TempHumControl -> SensirionHumidity;
  LeafNodeM.Humidity -> SensirionHumidity.Humidity;
  LeafNodeM.Temperature -> SensirionHumidity.Temperature;
  LeafNodeM.TemperatureError -> SensirionHumidity.TemperatureError;
  
  // Wiring for XMesh
  LeafNodeM.RouteControl -> MULTIHOPROUTER;
  LeafNodeM.Send -> MULTIHOPROUTER.MhopSend[AM_XMULTIHOP_MSG];
  MULTIHOPROUTER.ReceiveMsg[AM_XMULTIHOP_MSG] ->Comm.ReceiveMsg[AM_XMULTIHOP_MSG];   
}

