/*
 * LeafNodeM.nc - Implementation for leaf node
 * Team member: 1.Thai Tuan
 *              2. Duy Huynh
 *              3. Rithea Ngeth
 *              4. HO Tuan Vu
 * JAIST, Jul 9, 2016
 */
 
#include "appFeatures.h"
#define TIMER_PERIOD 1000	// 1s period
includes MultiHop;
//includes sensorboard;

/**
 * This module shows how to use the Timer, LED, ADC and XMesh components.
 * Sensor messages are sent multi-hop over the RF radio
 *
 * @author Crossbow Technology Inc.
 **/
module ImdNodeM {
  provides {
    interface StdControl;
  }
  uses {  
    interface Leds;
	interface Timer;
	// Communication
	interface MhopSend as Send;
	interface RouteControl;	
	
  }
}
implementation {
   /**
   * Initialize the component.
   * 
   * @return Always returns <code>SUCCESS</code>
   **/
  command result_t StdControl.init() {
    uint16_t len;
	
    call Leds.init();				// Init Leds 
	
    return SUCCESS;
  }

  /**
   * Start things up.  This just sets the rate for the clock component.
   * 
   * @return Always returns <code>SUCCESS</code>
   **/
  command result_t StdControl.start() {
	call Timer.start(TIMER_REPEAT, TIMER_PERIOD);	// Start a repeating timer that fires every 1000ms  
	return SUCCESS;
  }

  /**
   * Halt execution of the application.
   * This just disables the clock component.
   * 
   * @return Always returns <code>SUCCESS</code>
   **/
  command result_t StdControl.stop() {
	call Timer.stop();
	return SUCCESS;
  }
  
  event result_t Send.sendDone(TOS_MsgPtr msg, result_t success) {
	return SUCCESS;
  }
  
  event result_t Timer.fired(){
	call Leds.redToggle();
	return SUCCESS;
  }
}


