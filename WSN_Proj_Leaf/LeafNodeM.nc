/*
 * LeafNodeM.nc - Implementation for leaf node
 * Team member: 1.Thai Tuan
 *              2. Duy Huynh
 *              3. Rithea Ngeth
 *              4. HO Tuan Vu
 * JAIST, Jul 9, 2016
 */
 
#include "appFeatures.h"
#define TIMER_PERIOD 5000	// 5s period
includes MultiHop;
//includes sensorboard;

/**
 * This module shows how to use the Timer, LED, ADC and XMesh components.
 * Sensor messages are sent multi-hop over the RF radio
 *
 * @author Crossbow Technology Inc.
 **/
module LeafNodeM {
  provides {
    interface StdControl;
  }
  uses {
    interface Timer;
    interface Leds;
	
	// Communication
	interface MhopSend as Send;
	interface RouteControl;
	
	// Battery
	interface StdControl as BattControl;
	interface ADC as ADCBatt;	
	
	// Sensirion sensor
	interface SplitControl as TempHumControl;
	interface ADC as Temperature;
	interface ADC as Humidity;
	interface ADCError as TemperatureError;
  }
}
implementation {
  bool sending_packet = FALSE;
  bool sensing_process = FALSE;
  TOS_Msg msg_buffer;
  XDataMsg *pack;
  
  /**
   * Initialize the component.
   * 
   * @return Always returns <code>SUCCESS</code>
   **/
  command result_t StdControl.init() {
    uint16_t len;
	
    call Leds.init();				// Init Leds
	call BattControl.init();		// Init Battery Ref
	call TempHumControl.init();	// Init Sensirion

    // Initialize the message packet with default values
    atomic {
	  sending_packet = FALSE;
	  sensing_process = FALSE;
      pack = (XDataMsg*)call Send.getBuffer(&msg_buffer, &len);
      pack->board_id = SENSOR_BOARD_ID;
      pack->packet_id = 0;
	}
	
    return SUCCESS;
  }

  /**
   * Start things up.  This just sets the rate for the clock component.
   * 
   * @return Always returns <code>SUCCESS</code>
   **/
  command result_t StdControl.start() {
    call BattControl.start();
	//call TemperatureError.enable();
	call Timer.start(TIMER_REPEAT, TIMER_PERIOD);	// Start a repeating timer that fires every 1000ms
	return SUCCESS;
  }

  /**
   * Halt execution of the application.
   * This just disables the clock component.
   * 
   * @return Always returns <code>SUCCESS</code>
   **/
  command result_t StdControl.stop() {
	call BattControl.stop();
    call Timer.stop();
	return SUCCESS;
  }
  
    /**
   * Toggle the red LED in response to the <code>Timer.fired</code> event.  
   * Start the Light sensor control and sample the data
   *
   * @return Always returns <code>SUCCESS</code>
   **/
  event result_t Timer.fired()
  {
   	call TempHumControl.start();
    return SUCCESS;
  }

  void task SendData()
  {
    if (sending_packet) return;
    atomic sending_packet = TRUE;
	call Leds.redOn();    
	// send message to XMesh multi-hop networking layer
    pack->parent = call RouteControl.getParent();
	atomic pack->packet_id++;
    if (call Send.send(BASE_STATION_ADDRESS,MODE_UPSTREAM,&msg_buffer,sizeof(XDataMsg)) != SUCCESS)
	  sending_packet = FALSE;
	return;
  }
  
  void task stopTempHumpControl(){
	call TempHumControl.stop();
  }
  
 /**
   * Sensor data has been sucessfully sent through XMesh
   * Toggle green LED to signal message sent
   *
   * @return Always returns <code>SUCCESS</code>
   **/ 
  event result_t Send.sendDone(TOS_MsgPtr msg, result_t success) {
    call Leds.redOff();
    atomic sending_packet = FALSE;
	return SUCCESS;
  }
  
  // TemHumControl event
  async event result_t Temperature.dataReady(uint16_t data) {
	atomic pack->temperature = data;
	call Leds.greenOff();
	call Leds.yellowOn();
	return call ADCBatt.getData();
  }
  event result_t TempHumControl.startDone(){
    atomic if(!sensing_process){
	  atomic sensing_process = TRUE;
	  call Leds.greenOn();
	  call Temperature.getData();
	}
	return SUCCESS;
  }
  
  event result_t TempHumControl.initDone(){
	return SUCCESS;
  }

  event result_t TempHumControl.stopDone(){
	return SUCCESS;
  }
  
  async event result_t ADCBatt.dataReady(uint16_t data){
	atomic pack->vref = data;		// VBat = 1252352/data
	atomic sensing_process = FALSE;
	call Leds.yellowOff();	
	post SendData();
	return SUCCESS;
  }
  event result_t TemperatureError.error(uint8_t token){
	call TempHumControl.stop();
	return SUCCESS;
  }
  
  async event result_t Humidity.dataReady(uint16_t data){
	//pack->xData.data1.humidity = data ;
	//atomic sensor_state = SENSOR_HUMIDITY_GETTEMPDATA;
	return SUCCESS;
  }
}


